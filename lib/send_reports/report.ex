defmodule SendReports.Report do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "reports" do
    field :shipping_time, :string
    field :shipping, :boolean, default: false
    timestamps()
  end

  def changeset(report, attrs \\ %{}) do
    report
    |> cast(attrs, [:shipping])
  end
end