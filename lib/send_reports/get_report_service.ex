defmodule SendReports.GetReportService do
  alias SendReports.{Repo, Report}
  use HTTPoison.Base
  

  def get_report do
    file_name = "products_report.csv"
    path = Path.join(System.tmp_dir!(),file_name)
    report_to_send = Repo.get_by(SendReports.Report, shipping: false)
    if (report_to_send != nil) do
       send_report(file_name, path, report_to_send)
       {:ok}
    else
      {:error}
    end     
  end

  def send_report(file_name, path, report_to_send) do
    with {:ok ,_} <- HTTPoison.post(
      "https://api.mailgun.net/v3/sandbox4938af71925644cd8f9711cef31eac3b.mailgun.org/messages", 
      {:multipart, [
          {"from","cadprod@sandbox4938af71925644cd8f9711cef31eac3b.mailgun.org"},
          {"to", "gabriel.zuqueto@skyhub.com.br"},
          {"cc", "marcos.nogueira@skyhub.com.br"},
          {"text", "Segue Relatorio"},
          {"attachment", IO.iodata_to_binary(File.read!(path)),
            {"form-data", [
              {"name", "\"attachment\""},
              {"filename", file_name}]},
            [{"Content-Type", "text/csv"}]}]},
      [{"Authorization", 
              "Basic YXBpOjI1YzcyMDBiYThiMjY1Y2EyNmNlOThiOWVhMjg0NDkxLWQ1ZTY5YjBiLTAzZmZkNjg5"}]) do
      update(report_to_send)          
    end
    {:ok}
  end

  def update(report_to_send) do
    with changeset <- Report.changeset(report_to_send, %{shipping: true}) do
         Repo.update!(changeset)
    end 
  end
end