# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :send_reports,
  ecto_repos: [SendReports.Repo]

# Configures the endpoint
config :send_reports, SendReportsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "FpQmogTB035hgvSf6x+wxAGih3FVAsPLYFMQdu4/3vAp6Ft23BXVqUYcrqN9EZ8f",
  render_errors: [view: SendReportsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SendReports.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.




import_config "#{Mix.env}.exs"
