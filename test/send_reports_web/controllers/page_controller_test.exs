defmodule SendReportsWeb.PageControllerTest do
  use SendReportsWeb.ConnCase


  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Bem Vindo ao Send_Report!"
  end

  test "render index when there is a report", %{conn: conn} do
    with {:ok, conn} <- get conn, "/report" do
      assert html_response(conn, 200) =~ "Relatorio Enviado"
    end
  end

  test "render index2 when there is no a report", %{conn: conn} do
    with {:error, conn} <- get conn, "/report" do
      assert html_response(conn, 200) =~ "Nao ha Relatorio"
    end
  end

end
