defmodule SendReportsWeb.PageController do
  use SendReportsWeb, :controller

  alias SendReports.GetReportService

  def index(conn, _params) do
    with {:ok} <- GetReportService.get_report() do
      render(conn, "index.html")
    else
      {:error} ->
       render(conn, "index2.html")
     end    
   end
end
